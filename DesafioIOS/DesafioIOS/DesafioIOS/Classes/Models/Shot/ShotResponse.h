//
//  ShotResponse.h
//  DesafioIOS
//
//  Created by Leonardo Bonetti on 2/10/15.
//  Copyright (c) 2015 Leonardo Bonetti. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ShotResponse : NSObject

@property (nonatomic, strong) NSArray *shotList;

+ (id)initShotResponseWithDictionary:(NSDictionary *)dictionary;

@end
