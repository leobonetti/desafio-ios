//
//  Shot.h
//  DesafioIOS
//
//  Created by Leonardo Bonetti on 2/10/15.
//  Copyright (c) 2015 Leonardo Bonetti. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Player.h"

@interface Shot : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *description;
@property (nonatomic) int views_count;
@property (nonatomic, strong) NSString *image_url;
@property (nonatomic, strong) Player *player;

@end
