//
//  Player.h
//  DesafioIOS
//
//  Created by Leonardo Bonetti on 2/10/15.
//  Copyright (c) 2015 Leonardo Bonetti. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Player : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *avatar_url;

@end
