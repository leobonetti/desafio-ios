//
//  Player.m
//  DesafioIOS
//
//  Created by Leonardo Bonetti on 2/10/15.
//  Copyright (c) 2015 Leonardo Bonetti. All rights reserved.
//

#import "Player.h"
#import "NSObject+OSReflectionKit.h"

@implementation Player

@synthesize name, avatar_url;

@end
