//
//  ShotResponse.m
//  DesafioIOS
//
//  Created by Leonardo Bonetti on 2/10/15.
//  Copyright (c) 2015 Leonardo Bonetti. All rights reserved.
//

#import "ShotResponse.h"
#import "NSObject+OSReflectionKit.h"
#import "Shot.h"

@implementation ShotResponse

@synthesize shotList;

+ (id)initShotResponseWithDictionary:(NSDictionary *)dictionary
{
    ShotResponse *shotResponse = [ShotResponse objectFromDictionary:dictionary];
    
    return shotResponse;
}

+ (NSDictionary *)reflectionMapping
{
    return @{@"shots": @"shots,*"};
}

- (void)reflectionTranformsValue:(id)value forKey:(NSString *)propertyName
{
    if([propertyName isEqualToString:@"shots"])
    {
        NSArray *shotsListAux = [NSArray arrayWithArray:(NSArray *)value];
        NSMutableArray *shotsList = [NSMutableArray array];
        
        for(NSDictionary *dicShot in shotsListAux)
        {
            Shot *shot = [Shot objectFromDictionary:dicShot];
            [shotsList addObject:shot];
        }
        
        self.shotList = [NSArray arrayWithArray:shotsList];
    }
}

@end
