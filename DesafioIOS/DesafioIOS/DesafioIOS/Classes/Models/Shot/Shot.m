//
//  Shot.m
//  DesafioIOS
//
//  Created by Leonardo Bonetti on 2/10/15.
//  Copyright (c) 2015 Leonardo Bonetti. All rights reserved.
//

#import "Shot.h"
#import "NSObject+OSReflectionKit.h"

@implementation Shot

@synthesize title, description, image_url;
@synthesize views_count;
@synthesize player;

+ (NSDictionary *)reflectionMapping
{
    return @{@"player":@"player,*"};
}

- (void)reflectionTranformsValue:(id)value forKey:(NSString *)propertyName
{
    if([propertyName isEqualToString:@"player"])
    {
        self.player = [Player objectFromDictionary:(NSDictionary *)value];
    }
}

@end
