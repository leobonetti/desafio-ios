//
//  VCShotList.h
//  DesafioIOS
//
//  Created by Leonardo Bonetti on 2/10/15.
//  Copyright (c) 2015 Leonardo Bonetti. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VCShotList : UIViewController<UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *cvShotList;
@property (weak, nonatomic) IBOutlet UILabel *lbLoading;

@end
