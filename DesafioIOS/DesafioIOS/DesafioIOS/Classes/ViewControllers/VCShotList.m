//
//  VCShotList.m
//  DesafioIOS
//
//  Created by Leonardo Bonetti on 2/10/15.
//  Copyright (c) 2015 Leonardo Bonetti. All rights reserved.
//

#import "VCShotList.h"
#import "cvShotListCell.h"
#import "AFHTTPRequestOperationManager.h"
#import "ShotResponse.h"
#import "Shot.h"
#import "Player.h"
#import "WSDefinitions.h"
#import "VCShotListDefinitions.h"

@interface VCShotList ()

@property (nonatomic) int pageNumber;
@property (nonatomic) bool loading;
@property (nonatomic, strong) NSIndexPath *selectedIndex;
@property (nonatomic, strong) NSMutableArray *shotList;

@end

@implementation VCShotList

@synthesize cvShotList;
@synthesize pageNumber;
@synthesize loading;
@synthesize selectedIndex;
@synthesize shotList;
@synthesize lbLoading;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initVariables];
    [self requestShotList];
    
    [self.cvShotList registerNib:[UINib nibWithNibName:@"ShotListCell" bundle:nil] forCellWithReuseIdentifier:CELL_IDENTIFIER];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Initialize variables

- (void)initVariables
{
    self.loading = NO;
    self.selectedIndex = nil;
    self.pageNumber = 1;
    self.shotList = [NSMutableArray array];
}

#pragma mark - API Call

- (void)requestShotList
{
    self.loading = YES;
    
    NSMutableURLRequest *urlRequest = [[AFHTTPRequestSerializer serializer] requestWithMethod:GET URLString:[NSString stringWithFormat:@"%@%@%@", API_BASE_URL, SHOT_SERVICE, SHOT_POPULAR] parameters:@{ PAGE: [NSString stringWithFormat:@"%d", self.pageNumber] } error:nil];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:urlRequest];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        self.lbLoading.hidden = YES;
        
        NSDictionary *dicResponse = (NSDictionary *)responseObject;
        ShotResponse *shotResponse = [ShotResponse initShotResponseWithDictionary:dicResponse];
        
        [self.shotList addObjectsFromArray:shotResponse.shotList];
        
        self.pageNumber++;
        [self.cvShotList reloadData];
        
        self.loading = NO;
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:ALERT_ERROR message:ERROR_MESSAGE delegate:nil cancelButtonTitle:CANCEL_BUTTON otherButtonTitles:nil, nil];
        [alert show];
    }];
    
    [operation start];
}

#pragma mark - UICollectionView DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.shotList count];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    cvShotListCell *cell = (cvShotListCell *)[collectionView dequeueReusableCellWithReuseIdentifier:CELL_IDENTIFIER forIndexPath:indexPath];
    
    Shot *shot = [self.shotList objectAtIndex:indexPath.row];

    [cell setUserImage:shot.image_url];
    [cell setTitle:shot.title];
    [cell setViews: shot.views_count];
    [cell setUserAvatar:shot.player.avatar_url];
    [cell setName: shot.player.name];
    [cell setDescription: shot.description];

    
    return cell;
}

#pragma mark - UICollectionView DelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize rect = CGSizeMake(300, 200);
    if([self.selectedIndex isEqual:indexPath])
        rect.height = 400;
    return rect;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 10, 30, 10);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 40;
}

#pragma mark - UICollectionView Delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if([self.selectedIndex isEqual:indexPath])
        self.selectedIndex = nil;
    else
        self.selectedIndex = indexPath;
    
    [collectionView reloadData];
}


#pragma mark - UIScrollView Delegate
/* UICollectionView Pagination Control */

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if(!self.loading)
    {
        float endScrolling = scrollView.contentOffset.y + scrollView.frame.size.height;
        if(endScrolling >= scrollView.contentSize.height - 200)
        {
            [self requestShotList];
        }
    }
}

@end
















