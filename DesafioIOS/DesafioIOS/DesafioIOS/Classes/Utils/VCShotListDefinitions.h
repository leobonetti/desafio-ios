//
//  VCShotListDefinitions.h
//  DesafioIOS
//
//  Created by Leonardo Bonetti on 2/11/15.
//  Copyright (c) 2015 Leonardo Bonetti. All rights reserved.
//

#pragma mark - Alert

#define ALERT_ERROR @"Error"
#define ERROR_MESSAGE @"Unable to handle request"
#define CANCEL_BUTTON @"OK"

#pragma mark - UIColletionView

#define CELL_IDENTIFIER @"cvShotListCellIdentifier"