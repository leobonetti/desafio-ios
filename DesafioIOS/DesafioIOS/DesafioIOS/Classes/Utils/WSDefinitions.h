//
//  WSDefinitions.h
//  DesafioIOS
//
//  Created by Leonardo Bonetti on 2/11/15.
//  Copyright (c) 2015 Leonardo Bonetti. All rights reserved.
//

#pragma mark - HTTP Request type

#define GET @"get"

#pragma mark - Dribbble API Base URL

#define API_BASE_URL @"https://api.dribbble.com/"

#pragma mark - Shots request

#define SHOT_SERVICE @"shots/"
#define SHOT_POPULAR @"popular"

#pragma mark - Shots popular params

#define PAGE @"page"