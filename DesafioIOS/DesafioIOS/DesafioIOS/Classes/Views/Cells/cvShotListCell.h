//
//  cvShotListCell.h
//  DesafioIOS
//
//  Created by Leonardo Bonetti on 2/10/15.
//  Copyright (c) 2015 Leonardo Bonetti. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface cvShotListCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgUser;
@property (weak, nonatomic) IBOutlet UIImageView *imgUserAvatar;

@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbView;
@property (weak, nonatomic) IBOutlet UILabel *lbName;

@property (weak, nonatomic) IBOutlet UIWebView *wvDescription;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *aivLoading;

- (void)setTitle:(NSString *)title;
- (void)setViews:(int)view;
- (void)setName:(NSString *)name;
- (void)setDescription:(NSString *)description;
- (void)setUserImage:(NSString *)imageUrl;
- (void)setUserAvatar:(NSString *)avatarURL;

@end
