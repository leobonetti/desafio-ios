//
//  cvShotListCell.m
//  DesafioIOS
//
//  Created by Leonardo Bonetti on 2/10/15.
//  Copyright (c) 2015 Leonardo Bonetti. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "cvShotListCell.h"
#import "SDWebImageManager.h"

@implementation cvShotListCell

@synthesize imgUser, imgUserAvatar;
@synthesize lbTitle, lbView, lbName;
@synthesize wvDescription;


- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    self.imgUserAvatar.layer.cornerRadius = 20;
    self.imgUserAvatar.layer.masksToBounds = YES;
    
    self.wvDescription.layer.borderColor = [[UIColor colorWithWhite:0.000 alpha:0.200] CGColor];
    self.wvDescription.layer.borderWidth = 1.0f;
    
    self.layer.borderColor = [[UIColor blackColor] CGColor];
    self.layer.borderWidth = 1.0f;
}

- (void)setTitle:(NSString *)title
{
    self.lbTitle.text = title;
}

- (void)setViews:(int)view
{
    self.lbView.text = [NSString stringWithFormat:@"%d", view];
}

- (void)setName:(NSString *)name
{
    self.lbName.text = name;
}

- (void)setDescription:(NSString *)description
{
    [self.wvDescription loadHTMLString:description baseURL:nil];
}

- (void)setUserImage:(NSString *)imageUrl
{
    self.imgUser.hidden = YES;
    self.aivLoading.hidden = NO;
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:[NSURL URLWithString:imageUrl] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        /* Nothing Here */
    } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
        self.imgUser.image = image;
        self.imgUser.hidden = NO;
        self.aivLoading.hidden = YES;
    }];
}

- (void)setUserAvatar:(NSString *)avatarURL
{
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:[NSURL URLWithString:avatarURL] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        /* Loading. */
    } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
        self.imgUserAvatar.image = image;
    }];
}

@end
