//
//  AppDelegate.h
//  DesafioIOS
//
//  Created by Leonardo Bonetti on 2/10/15.
//  Copyright (c) 2015 Leonardo Bonetti. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder

@property (nonatomic, retain) UIWindow *mainWindow;
@property (nonatomic, strong) UINavigationController *navController;


@end
